//
//  ViewController.swift
//  Swift2-OpenSSL-example
//
//  Created by tao on 6/29/15.
//  Copyright © 2015 TaoZhou. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		doSSL()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func doSSL() {
		SSL_load_error_strings()
		SSL_library_init()
		
		let my_ssl_method = TLSv1_2_client_method()
		let my_ssl_ctx = SSL_CTX_new(my_ssl_method)
		if my_ssl_ctx == nil {
			print("my_ssl_ctx nil")
			return
		}
		
		let my_ssl = SSL_new(my_ssl_ctx)
		if my_ssl == nil {
			print("my_ssl nil")
			return
		}
		
		let version = SSL_get_version(my_ssl)
		print(String.fromCString(version)!)
		
		let fd = self.connect("httpbin.org", port: "443")
		SSL_set_fd(my_ssl, fd)
		if (SSL_connect(my_ssl) <= 0) {
			print("connect failed")
			return
		}
		
		let req = "GET /ip HTTP/1.1\r\nHost: httpbin.org\r\n\r\n"
		SSL_write(my_ssl, req, 39)
		
		var buf = [CChar](count: 1024, repeatedValue: 0)
		SSL_read(my_ssl, &buf, 1024)
		
		print(String.fromCString(&buf)!)
		
		SSL_shutdown(my_ssl)
		SSL_free(my_ssl)
		SSL_CTX_free(my_ssl_ctx)
		close(fd)
	}
	
	func connect(host: String, port: String) -> Int32 {
		var hints = addrinfo()
		hints.ai_family = PF_UNSPEC
		hints.ai_socktype = SOCK_STREAM
		hints.ai_protocol = IPPROTO_TCP
		
		var results = UnsafeMutablePointer<addrinfo>()
		if getaddrinfo(host, port, &hints, &results) != 0 {
			return -1
		}
		
		var fd: Int32 = -1
		for var response = results; response != nil; response = response.memory.ai_next {
			let addr = response.memory
			
			fd = socket(addr.ai_family, addr.ai_socktype, addr.ai_protocol)
			if fd == -1 {
				continue
			}
			
			if Darwin.connect(fd, addr.ai_addr, addr.ai_addrlen) != -1 {
					break
			}
		}
		freeaddrinfo(results)
		
		return fd
	}
}