# Swift2-OpenSSL-example

##1. openssl.framework is from:
https://github.com/x2on/OpenSSL-for-iPhone

##2. fix the compile error of rsa.h:
http://stackoverflow.com/questions/24298632/compile-error-with-function-pointer-declaration
https://gist.github.com/letiemble/6710405

```
# Alter rsa.h to make Swift happy
sed -i .bak 's/const BIGNUM \*I/const BIGNUM *i/g' "$DIR/include/openssl/rsa.h"
```

